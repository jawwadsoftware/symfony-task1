<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserController extends AbstractController
{
	private $passwordHasher;
    /**
     * @Route("/user", name="user")
     */
    public function index(Request $request): Response
    {
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to the action: createUser(EntityManagerInterface $entityManager)
        $entityManager = $this->getDoctrine()->getManager();
		
		
		$repository = $this->getDoctrine()->getRepository(User::class);
		
		return $this->renderForm('user/index.html.twig', [
            
			'usersData' =>$repository->findAll()
        ]);
    }
	
	/**
     * @Route("/user/create", name="user_new")
     */
    public function new(Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to the action: createUser(EntityManagerInterface $entityManager)
        $entityManager = $this->getDoctrine()->getManager();
		
		$this->passwordHasher = $passwordHasher;

        $user = new User();
		
		
        $form = $this->createFormBuilder($user)
            ->add('email', TextType::class)
			->add('firstname', TextType::class)
			->add('lastname', TextType::class)
            ->add('password', TextType::class)
			->add('userRole', ChoiceType::class, [ 'choices'  => [
        'Select' => null,
        'ROLE_USER' => 'ROLE_USER',
        'ROLE_ADMIN' => 'ROLE_ADMIN',
    ],])
            ->add('save', SubmitType::class, ['label' => 'Create User'])
            ->getForm();
			
			$form->handleRequest($request);
			
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$user` variable has also been updated
            $user = $form->getData();
			$user->setPassword($this->passwordHasher->hashPassword(
            $user, $user->getPassword() ));
			$user->setRoles([$user->getUserRole()]);

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($user);
             $entityManager->flush();

            return $this->redirectToRoute('user_show', [
            'id' => $user->getId()
			]);
		}
		
		
		return $this->renderForm('user/new.html.twig', [
            'form' => $form,
			
        ]);
    }
	
	/**
     * @Route("/user/{id}", name="user_show")
     */
    public function show(User $user): Response
    {
        

        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id '.$id
            );
        }

        return new Response('Check out this great user: '.$user->getEmail());

        // or render a template
        // in the template, print things with {{ user.email }}
        // return $this->render('user/show.html.twig', ['user' => $user]);
    }
}
