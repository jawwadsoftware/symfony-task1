<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LuckyController extends AbstractController
{
	/** 
	 * @Route("/lucky/number")
	 */
    public function number(): Response
    {
		//$this->denyAccessUnlessGranted('ROLE_USER');
		$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
		
		//$hasAccess = $this->isGranted('ROLE_ADMIN');
		//$this->denyAccessUnlessGranted('ROLE_ADMIN');
        $number = random_int(0, 100);
		
        /** return new Response(
            '<html><body>Lucky number: '.$number.'</body></html>'
        );
		*/
		return $this->render('lucky/number.html.twig', [
            'number' => $number,
        ]);
    }
	/** 
	 * @Route("/lucky/number2")
	 */
	public function number2(): Response
    {
        $number = random_int(0, 100);

        return new Response(
            '<html><body>Lucky number2 is: '.$number.'</body></html>'
        );
    }
}