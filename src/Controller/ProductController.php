<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    
	
	/**
     * @Route("/product", name="create_product")
     */
    public function createProduct(Request $request): Response
    {
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to the action: createProduct(EntityManagerInterface $entityManager)
        $entityManager = $this->getDoctrine()->getManager();

        $product = new Product();
		
		
        $form = $this->createFormBuilder($product)
            ->add('name', TextType::class)
            ->add('price', TextType::class)
			->add('description', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Product'])
            ->getForm();
			
			$form->handleRequest($request);
			
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$product` variable has also been updated
            $product = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($product);
             $entityManager->flush();

            return $this->redirectToRoute('product_show', [
            'id' => $product->getId()
			]);
		}
		
		return $this->renderForm('product/new.html.twig', [
            'form' => $form,
        ]);
		
		/*
		$product->setName('Keyboard');
        $product->setPrice(1999);
        $product->setDescription('Ergonomic and stylish!');

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($product);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        return new Response('Saved new product with id '.$product->getId());
		*/
    }
	
	/**
     * @Route("/product/{id}", name="product_show")
     */
    public function show(Product $product): Response
    {
        

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        return new Response('Check out this great product: '.$product->getName());

        // or render a template
        // in the template, print things with {{ product.name }}
        // return $this->render('product/show.html.twig', ['product' => $product]);
    }
	
	/**
     * @Route("/product/edit/{id}")
     */
    public function update(int $id, Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
		$form = $this->createFormBuilder($product)
            ->add('name', TextType::class)
            ->add('price', TextType::class)
			->add('description', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Update Product'])
            ->getForm();
			
			$form->handleRequest($request);
			
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$product` variable has also been updated
            $product = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($product);
             $entityManager->flush();

            return $this->redirectToRoute('product_show', [
            'id' => $product->getId()
			]);
		}
        //$product->setName('New product name!');
        //$entityManager->flush();

        return $this->renderForm('product/new.html.twig', [
            'form' => $form,
        ]);
    }
}
